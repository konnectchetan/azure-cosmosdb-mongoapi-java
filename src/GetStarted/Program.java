package GetStarted;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

/**
 * Simple application that shows how to use Azure Cosmos DB for MongoDB API in a Java application.
 *
 */
public class Program {
	
    public static void main(String[] args)
    {
	/*
	* Replace connection string from the Azure Cosmos DB Portal
        */
        MongoClientURI uri = new MongoClientURI("YOUR_CONNECTION_STRING");
		
        MongoClient mongoClient = null;
        try {
            mongoClient = new MongoClient(uri);        
            
            // Get database
            MongoDatabase database = mongoClient.getDatabase("YOUR_DATABASE_NAME");

            // Get collection
            MongoCollection<Document> collection = database.getCollection("YOUR_COLLECTION_NAME");

            // Insert documents
            Document document1 = new Document("Training", "DevOps");
            collection.insertOne(document1);
            
            Document document2 = new Document("Training", "AWS");
            collection.insertOne(document2);           

            // Find items with filter
            Document queryResult = collection.find(Filters.eq("Training", "AWS")).first();
            System.out.println(queryResult.toJson());    	
        	
            System.out.println( "Completed successfully" );  
            
        } finally {
        	if (mongoClient != null) {
        		mongoClient.close();
        	}
        }
    }
}
