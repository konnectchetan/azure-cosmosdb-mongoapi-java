## Instructions
* Installing JDK 1.7+ and Maven 
    
    ```
    sudo su
    apt-get update
    apt-get install default-jdk maven
    ```
* Clone the code
    
    ```
    git clone https://gitlab.com/konnectchetan/azure-cosmosdb-mongoapi-java
    cd azure-cosmosdb-mongoapi-java
    ```
* To get the connection string
    - Go to your Cosomos DB page
    - In the left pane, click on `Quick Start`
    - Click on Java
    - There you will see your `Connection String`
* Update the `conenction string`, `database name`, and `collection name` in the Program.java file which is there in src directory
    
    ```vi src/GetStarted/Program.java```
* Compile the code and resolve dependinces

    ```mvn compile```
* Execute the code

    ```mvn exec:java -D exec.mainClass=GetStarted.Program```
